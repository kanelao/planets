import 'package:flutter/material.dart';
//import 'package:planets/ui/home/HomePage.dart';
import './gradientAppBar.dart';
import './homePageBody.dart';
import './detailPage.dart';

void main() {
  runApp(
    MaterialApp(
      title: 'Planets',
      home: MyHomePage(),
      routes: {
        '/detail': (_) => DetailPage(),
      },
    ),
  );
}

class MyHomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: new Column(
        children: <Widget>[
          GradientAppBar(title: "treva"),
          HomePageBody(),
        ],
      ),
    );
  }
}
