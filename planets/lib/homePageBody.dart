import 'package:flutter/material.dart';
import './planetSummary.dart';
import './planets.dart';

class HomePageBody extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return /** Expanded(
      child: ListView.builder(
        itemBuilder: (BuildContext context, int index) {
          return PlanetRow(planet: planets[index]);
        },
        itemCount: planets.length,
        padding: EdgeInsets.symmetric(vertical: 16.0),
      ),
    );*/ sliverList();
  }
}

 Widget sliverList(){
   return Expanded(
     child: Container(
       color: Color(0xFF736AB7),
       child: CustomScrollView(
         scrollDirection: Axis.vertical,
         slivers: <Widget>[
           SliverPadding(
             padding: EdgeInsets.symmetric(vertical: 24.0),
             sliver: SliverFixedExtentList(
               itemExtent: 152.0,
               delegate: SliverChildBuilderDelegate(
                 (context, index) => PlanetSummary(planet: planets[index]),
                 childCount: planets.length,
               ),
             ),
           )
         ],
       ),
     ),
   );
 }