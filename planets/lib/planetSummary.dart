import 'package:flutter/material.dart';
import './blueSeparator.dart';
import './planets.dart';
import './detailPage.dart';

class PlanetSummary extends StatelessWidget {
  final Planet planet;
  final bool horizontal;

  PlanetSummary({
    @required this.planet,
    this.horizontal = true,
  });

  PlanetSummary.vertical({
    @required this.planet,
  }) : this.horizontal = false;

  @override
  Widget build(BuildContext context) {
    final planetThumbnail = Container(
      margin: EdgeInsets.symmetric(vertical: 16.0),
      alignment: horizontal ? FractionalOffset.centerLeft:FractionalOffset.center,
      child: Hero(
        tag: "planet-hero-${planet.id}",
        child: Image(
          image: AssetImage(planet.image),
          height: 92.0,
          width: 92.0,
        ),
      ),
    );
    final baseTextStyle = TextStyle(fontFamily: 'Poppins');
    final headerTextStyle = baseTextStyle.copyWith(
      color: Colors.white,
      fontSize: 18.0,
      fontWeight: FontWeight.w600,
    );
    final regularTextStyle = baseTextStyle.copyWith(
        color: const Color(0xffb6b2df),
        fontSize: 9.0,
        fontWeight: FontWeight.w400);
    final subHeaderTextStyle = regularTextStyle.copyWith(fontSize: 12.0);
    Widget _planetValue(String value, String image) {
      return Expanded(
        child: Row(children: <Widget>[
          Image.asset(image, height: 12.0),
          Container(width: 8.0),
          Text(value, style: regularTextStyle),
        ]),
      );
    }

    final planetCardContent = new Container(
      margin: horizontal ? EdgeInsets.fromLTRB(76.0, 16.0, 16.0, 16.0) : EdgeInsets.fromLTRB(16.0, 42.0, 16.0, 16.0),
      constraints: BoxConstraints.expand(),
      child: Column(
        crossAxisAlignment: horizontal ? CrossAxisAlignment.start : CrossAxisAlignment.center,
        children: <Widget>[
          SizedBox(height: 4.0),
          Text(
            planet.name,
            style: headerTextStyle,
          ),
          SizedBox(height: 10.0),
          Text(
            planet.location,
            style: subHeaderTextStyle,
          ),
          BlueSeparator(),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              _planetValue(planet.distance, "assets/ic_distance.png"),
              Container(width: 32.0,),
                 _planetValue(planet.gravity, 'assets/ic_gravity.png'),
            ],
          ),
        ],
      ),
    );

    final planetCard = Container(
      height: horizontal ? 124.0 : 154.0,
      margin: horizontal ? EdgeInsets.only(left: 46.0) : EdgeInsets.only(top: 72.0),
      decoration: BoxDecoration(
          color: Color(0xFF333366),
          shape: BoxShape.rectangle,
          borderRadius: BorderRadius.circular(8.0),
          boxShadow: <BoxShadow>[
            BoxShadow(
                color: Colors.black12,
                blurRadius: 10.0,
                offset: Offset(0.0, 10.0))
          ]),
      child: planetCardContent,
    );

    return GestureDetector(
      onTap: horizontal ? () {
        Navigator.of(context).push(PageRouteBuilder(
          pageBuilder: (_, __, ___) => DetailPage(planet: planet),
        ));
      } : null,
      child: Container(
        margin: EdgeInsets.symmetric(
          horizontal: 24.0,
          vertical: 16.0,
        ),
        child: Stack(
          children: <Widget>[
            planetCard,
            planetThumbnail,
          ],
        ),
      ),
    );
  }
}
